import json
from os import path

import cv2
import matplotlib.image as mpimg
import pandas as pd
import tensorflow as tf
from keras.layers import Convolution2D, Flatten
from keras.layers import np
from keras.layers.core import Dense, Dropout
from keras.models import Sequential
from keras.optimizers import Adam
from keras.utils.visualize_util import plot
from sklearn.utils import shuffle

tf.python.control_flow_ops = tf

batch_size = 32
target_width = 64
target_height = 64


def load_data():
    rides = [
        "rides/ride_1",
        # "rides/ride_4",
        # "rides/ride_5",
        # "rides/ride_7",
        # "rides/ride_8",
        # "rides/ride_9",
        # "rides/ride_10",
        # "rides/ride_11",
        # "rides/ride_12",
        # "rides/ride_13",
    ]

    raw = []
    for idx, ride in enumerate(rides):
        df = pd.read_csv(path.join(ride, "driving_log.csv"),
                         header=None,
                         names=["center", "left", "right", "steering", "throttle", "brake", "speed"])
        raw.append(df)

    ride_data = pd.concat(raw, ignore_index=True)

    # filtered = ride_data['speed'] >= 18
    # ride_data = ride_data[filtered].reset_index()
    # print(ride_data.describe())

    return shuffle(ride_data[ride_data['throttle'] != 0])


def batch_generator(data, batch_size):
    images = np.zeros((batch_size, target_height, target_width, 3))
    angles = np.zeros(batch_size)
    total = len(data)
    cur = 0

    # actual loop that gets a random image and processes it before training
    while 1:
        for i in range(batch_size):
            row = data.iloc[cur]

            image, steering = process_row(row)

            images[i] = image
            angles[i] = steering
            cur = (cur + 1) % total

        yield images, angles


def get_image(filep):
    # the default data set had a different path than my own
    if "ride" not in filep:
        last_component = filep.rsplit("/", 1).pop()
        image = mpimg.imread("rides/ride_1/IMG/" + last_component)
    else:
        image = mpimg.imread(filep)
    return image


def process_row(line_data):
    # orig steering angle
    steering_angle = line_data['steering']

    # randomly decide on cam
    angle_type = np.random.randint(3)
    if angle_type == 0:
        file = line_data['left'].strip()
        steering_angle += .25
    elif angle_type == 1:
        file = line_data['center'].strip()
    else:
        file = line_data['right'].strip()
        steering_angle -= .25

    # load image
    image = get_image(file)
    # add some random translation
    image, steering_angle = augment_translate(image, steering_angle)

    # flip 50% of the times
    if np.random.randint(2) == 0:
        image = cv2.flip(image, 1)
        steering_angle = -steering_angle

    # crop the translated image
    image = process_image(image)

    return image, steering_angle


def augment_translate(image, steering_angle, x_trans=100):
    # thanks to: https://medium.com/@ValipourMojtaba/my-approach-for-project-3-2545578a9319#.af8lh8c4g
    # and https://github.com/vxy10/P3-BehaviorCloning/blob/master/model.py
    rows, cols, _ = image.shape
    x = x_trans * np.random.uniform() - x_trans / 2
    steering_angle += x / x_trans * 2 * 0.4
    y = 10 * np.random.uniform() - 10 / 2
    matrix = np.float32([[1, 0, x], [0, 1, y]])
    image = cv2.warpAffine(image, matrix, (cols, rows))
    return image, steering_angle


def process_image(img):
    img = img[65:135, :, :]
    img = cv2.resize(img, (target_width, target_height), interpolation=cv2.INTER_AREA)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    return img / 255 - 0.5


def configure_model():
    input_shape = (target_height, target_width, 3)
    dropout = .6

    model = Sequential()

    model.add(Convolution2D(24, 5, 5, init='he_normal', subsample=(2, 2), activation='relu', input_shape=input_shape))
    model.add(Convolution2D(36, 5, 5, init='he_normal', subsample=(2, 2), activation='relu'))
    model.add(Convolution2D(48, 5, 5, init='he_normal', subsample=(2, 2), activation='relu'))
    model.add(Convolution2D(64, 3, 3, init='he_normal', subsample=(1, 1), activation='relu'))
    model.add(Convolution2D(64, 3, 3, init='he_normal', subsample=(1, 1), activation='relu'))

    model.add(Flatten())

    model.add(Dense(1164, init='he_normal', activation='relu'))
    model.add(Dropout(dropout))

    model.add(Dense(100, init='he_normal', activation='relu'))
    model.add(Dropout(dropout))

    model.add(Dense(50, init='he_normal', activation='relu'))
    model.add(Dropout(dropout))

    model.add(Dense(10, init='he_normal', activation='relu'))

    model.add(Dense(1, init='he_normal'))

    return model


def fit_generator(model, nb_epoch, train_generator, train_size, val_generator, val_size):
    model.compile(loss="mse", optimizer=Adam(lr=0.0001))
    model.fit_generator(train_generator,
                        samples_per_epoch=train_size,
                        nb_epoch=nb_epoch,
                        validation_data=val_generator,
                        nb_val_samples=val_size,
                        verbose=1)


def save_model(keras_model):
    json.dump(keras_model.to_json(), open("model.json", "w"))
    keras_model.save_weights("model.h5")


def save_model_visual(keras_model):
    plot(keras_model, to_file='nvidia.png')


def print_model_summary(keras_model):
    keras_model.summary()


# just used to created the images in the README.md
def save_images_for_visualization():
    row = {'steering': 0.15,
           'left': 'center_2016_12_01_13_31_15_411.jpg',
           'center': 'center_2016_12_01_13_31_15_411.jpg',
           'right': 'center_2016_12_01_13_31_15_411.jpg'}
    img, steer = process_row(row)
    mpimg.imsave("out.jpg", img)


if __name__ == '__main__':
    train_data = load_data()

    # split into train and val set
    split = int(len(train_data) * 0.9) // batch_size * batch_size
    trains = train_data[:split]
    val_data = train_data[split:]
    new_val = len(val_data) // batch_size * batch_size
    val_data = val_data[:new_val]

    # use the same generator even though it tempers the image
    # but the loss is unreliable anyway :)
    train_generator = batch_generator(trains, batch_size)
    valid_generator = batch_generator(val_data, batch_size)

    # model and meta
    model = configure_model()
    save_model_visual(model)
    print_model_summary(model)

    fit_generator(model, 1, train_generator, 28800, valid_generator, int(len(trains) / batch_size / 10.0))
    save_model(model)
